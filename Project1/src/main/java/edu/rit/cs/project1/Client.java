package edu.rit.cs.project1;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Client {
    private static void PrintStatus(String status) {
        System.out.println("\t<Client: " + status + ">");
    }

    private static final int kDataArgIdx = 0;
    private static final int kCountArgIdx = 1;

    private static final int kAddressArgIdx = 2;
    private static final int kServerPortArgIdx = 3;
    private static final int kReturnPortArgIdx = 4;


    public static void main(String[] args) {
        //===========================================<Argument Parsing>===========================================//
        //============================================<Data File>=============================================//
        if (args.length <= kDataArgIdx) {
            System.out.println("Usage Error: you must specify a review file to parse");
            System.exit(1);
        }
        String dataPath = args[kDataArgIdx];

        //============================================<Count File>============================================//
        if (args.length <= kCountArgIdx) {
            System.out.println("Usage Error: you must specify a review file to parse");
            System.exit(1);
        }
        String countPath = args[kCountArgIdx];

        //==========================================<Optional Args>===========================================//
        String address = Handler.kDefServerAddress;
        if (args.length > kAddressArgIdx) {
            address = args[kAddressArgIdx];
        }

        int serverPort = Handler.kDefServerPort;
        if (args.length > kServerPortArgIdx) {
            try {
                serverPort = Integer.parseInt(args[kServerPortArgIdx]);
            } catch (NumberFormatException e) {
                System.out.println("Server Port: Invalid port number \"" + args[kServerPortArgIdx] + "\" specified. Using default.");
                serverPort = Handler.kDefServerPort;
            }
        }

        int retPort = Handler.kDefRetPort;
        if (args.length > kReturnPortArgIdx) {
            try {
                retPort = Integer.parseInt(args[kReturnPortArgIdx]);
            } catch (NumberFormatException e) {
                System.out.println("Return Port: Invalid port number \"" + args[kReturnPortArgIdx] + "\" specified. Using default.");
                retPort = Handler.kDefRetPort;
            }
        }


        //============================================<Send Data File>============================================//
        System.out.println("<Opened Client>");

        Handler handler = null;
        boolean gotException = true;
        try {
            //Open a connection to the count server
            handler = new Handler(new Socket(address, serverPort));
            PrintStatus("Connected to server");

            //Send Return port and  data file bytes
            handler.sendRequest(new Request(Handler.kReturnPortLabel, String.valueOf(retPort)));
            PrintStatus("Sent return port");

            PrintStatus("Sending data file \"" + dataPath + "\" to server");
            handler.sendFile(dataPath);
            PrintStatus("data file sent");

            gotException = false;
        } catch (IOException e) {
            System.out.println("IO: " + e.getMessage());
        } finally {
            //Close and null out the handler
            if (handler != null)
                handler.close();
            handler = null;
        }
        if (gotException) System.exit(1);

        PrintStatus("Disconnected from server");

        //============================================<Get Count File>============================================//
        //===================================<Get a New Network Connection>===================================//
        PrintStatus("Waiting for server to reconnect");

        Socket serverReply = null; //The socket that the server will reply on

        // Get a response socket from the server
        try (ServerSocket listenSocket = new ServerSocket(retPort)) {
            serverReply = listenSocket.accept();
        } catch (IOException e) {
            //
            System.out.println("Return Listen: " + e.getMessage());
            if (serverReply != null) {
                try {
                    serverReply.close();
                } catch (IOException ignored) {
                    /*Failed to close the reply socket*/
                }

                //Mark the reply socket as null
                serverReply = null;
            }
        }

        // If we failed to connect, exit prematurely
        if (serverReply == null) {
            System.out.println("<Closed Client>");
            System.exit(1);
        }

        PrintStatus("Server reconnected");

        //========================================<Get the Count File>========================================//
        try {
            //Create a new handler to do socket *stuff* with the server again
            handler = new Handler(serverReply);

            //Get the count file from the server and save to the specified count file
            PrintStatus("Getting count file from server");
            handler.getFile(countPath);
            PrintStatus("Saved count file as \"" + countPath + "\"");

        } catch (IOException e) {
            System.out.println();
        } finally {
            if (handler != null) {
                handler.close();
            }
        }

        //=========================================<Print the count file>=========================================//
        try (BufferedReader fileReader = new BufferedReader(new FileReader(countPath))) {
            String nextLine = fileReader.readLine();
            while (nextLine != null) {
                System.out.println(nextLine);
                nextLine = fileReader.readLine();
            }
        } catch (IOException e) {
            System.out.println("Count File Print: " + e.getMessage());
        }


        //===============================================<Cleanup>================================================//
        System.out.println("<Closed Client>");
    }
}
