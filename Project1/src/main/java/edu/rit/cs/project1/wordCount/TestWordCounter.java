package edu.rit.cs.project1.wordCount;

import java.io.IOException;
import java.util.Map;

public class TestWordCounter {

    public static void main(String[] args) throws IOException {
        WordCounter.WordCountReturn returnBlock = WordCounter.countWords(args[0]);
        Map<String, Integer> counts = returnBlock.getCounts();

        System.out.println("map() time was " + returnBlock.getMapTime() + " seconds");
        System.out.println("reduce() time was " + returnBlock.getReduceTime() + " seconds");

        System.out.println("Counts for " + args[0] + ":");
        for(String word: counts.keySet()) {
            System.out.println("\t" + word + ":" + counts.get(word));
        }
    }
}
