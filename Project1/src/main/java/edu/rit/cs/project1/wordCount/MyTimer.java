package edu.rit.cs.project1.wordCount;

public class MyTimer {
    private long startTime;
    private long elapsedTime;

    public MyTimer() {
        this.elapsedTime = 0;
    }

    public void start_timer() {
        this.startTime = System.nanoTime();
    }

    public void stop_timer() {
        this.elapsedTime += System.nanoTime() - this.startTime;
    }

    public double getElapsedTime() {
        return (double) this.elapsedTime / 1_000_000_000;
    }

    public void print_elapsed_time(String timerName) {
        double elapsedTimeInSecond = (double) this.elapsedTime / 1_000_000_000;
        System.out.println("ElapsedTime (" + timerName + "): " + elapsedTimeInSecond + " seconds");
    }

}
