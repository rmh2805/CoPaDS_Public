package edu.rit.cs.project1;


import java.nio.charset.StandardCharsets;

public class Request {
    private String label;
    private String args;

    private static String substringBetween(String str, char startCh, char endCh) {
        int startIdx = str.indexOf(startCh);
        int endIdx = str.lastIndexOf(endCh);
        if(startIdx == -1 || endIdx == -1 || startIdx >= endIdx) {
            return null;
        }

        return str.substring(startIdx + 1, endIdx);
    }

    static Request rebuildRequest(String stringIn) {
        stringIn = stringIn.trim(); //Trim off the whitespace
        if (!stringIn.startsWith(Request.class.getSimpleName())) return null;  // Check that we can cast this properly


        //Grab the contents of this request, if incomplete, return null
        String myBlock = substringBetween(stringIn, '[', ']');
        if(myBlock == null) return null;

        //Split up the data block, return null if incomplete
        String[] data = myBlock.split(",", 2);
        if(data.length < 2) return null;


        //Grab the label and argument strings, return null if either is missing
        String label = substringBetween(data[0], '"', '"');
        String args = substringBetween(data[1], '"', '"');
        if(label == null || args == null) return null;

        //Return a new request with the parsed label and arguments
        return new Request(label, args);
    }

    public Request(String label, String args) {
        this.label = label;
        this.args = args;

        if(label == null) this.label = "";
        if(args == null) this.args = "";
    }


    public String getLabel() {
        return label;
    }

    public String getArgs() {
        return args;
    }

    public String toString(){
        String toReturn = this.getClass().getSimpleName() + ":[\"" + this.label + "\",\"" +
                this.args + "\"]";
        return new String(toReturn.getBytes(), StandardCharsets.UTF_8);
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof Request))
            return false;
        Request o = (Request) obj;
        return o.getArgs().equals(this.args) && o.getLabel().equals(this.label);
    }
}
