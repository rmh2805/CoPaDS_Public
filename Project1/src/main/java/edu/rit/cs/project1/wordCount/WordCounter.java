package edu.rit.cs.project1.wordCount;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordCounter {
    public static class WordCountReturn {
        private Map<String, Integer> counts;
        private double mapTime;
        private double reduceTime;

        public WordCountReturn(Map<String, Integer> counts, double mapTime, double reduceTime) {
            this.counts = counts;
            this.mapTime = mapTime;
            this.reduceTime = reduceTime;
        }

        public Map<String, Integer> getCounts() {
            return counts;
        }

        public double getMapTime() {
            return mapTime;
        }

        public double getReduceTime() {
            return reduceTime;
        }
    }

    private static final int kMaxReviews = 0xF00; //The max number of reviews to read before reduction

    /**
     * Reads all lines from review file "dataFile" and performs a word count on them
     *
     * @param dataFile The file to count
     * @return A map from words to their counts
     */
    public static WordCountReturn countWords(String dataFile) throws IOException {
        Map<String, Integer> counts = new HashMap<>();
        MyTimer mapTimer = new MyTimer(), reduceTimer = new MyTimer();

        try(BufferedReader dataReader = new BufferedReader(new FileReader(dataFile))) {
            dataReader.readLine(); //Read the header line

            //Grab the first batch of reviews
            List<AmazonFineFoodReview> reviews = readNReviews(dataReader, kMaxReviews);

            //While there are still reviews to grab
            while(reviews != null) {
                mapTimer.start_timer();
                List<KV<String, Integer>> kv_pairs = map(reviews);
                mapTimer.stop_timer();

                reduceTimer.start_timer();
                reduce(counts, kv_pairs);
                reduceTimer.stop_timer();

                reviews = readNReviews(dataReader, kMaxReviews);
            }

        }
        return new WordCountReturn(counts, mapTimer.getElapsedTime(), reduceTimer.getElapsedTime());
    }

    public static Map<String, Integer> getWordCount(String dataFile) throws IOException {
        return countWords(dataFile).getCounts();
    }

    /**
     * Emit 1 for every word and store this as a KV pair
     * (Lifted directly from basic_word_count)
     *
     * @param reviews A list of reviews to parse
     * @return The list of emitted KV pairs
     */
    private static List<KV<String, Integer>> map(List<AmazonFineFoodReview> reviews) {
        List<KV<String, Integer>> kv_pairs = new ArrayList<KV<String, Integer>>();
        for(AmazonFineFoodReview review : reviews) {
            Pattern pattern = Pattern.compile("([a-zA-Z]+)");
            Matcher matcher = pattern.matcher(review.get_Summary());

            while(matcher.find())
                kv_pairs.add(new KV<>(matcher.group().toLowerCase(), 1));
        }
        return kv_pairs;
    }

    /**
     * Counts the frequency of each unique word and updates the passed workingMap
     * (Lifted directly from basic_word_count, with minor change to update instead of generate map)
     *
     * @param workingMap The current map of words to counts, to be updated
     * @param kv_pairs The list of KV pairs to reduce
     */
    private static void reduce(Map<String, Integer> workingMap, List<KV<String, Integer>> kv_pairs) {
        for(KV<String, Integer> kv : kv_pairs) {
            if(!workingMap.containsKey(kv.getKey())) {
                workingMap.put(kv.getKey(), kv.getValue());
            } else{
                int init_value = workingMap.get(kv.getKey());
                workingMap.replace(kv.getKey(), init_value, init_value+kv.getValue());
            }
        }
    }

    /**
     * Reads and parses the next 'nReviews' reviews (fewer if EOF reached beforehand)
     * @param fileReader A buffered reader on the data file
     * @param nReviews The maximum number of reviews to read
     * @return List of reviews, null if none were grabbed
     */
    private static List<AmazonFineFoodReview> readNReviews(BufferedReader fileReader, int nReviews) throws IOException {
        List<AmazonFineFoodReview> reviews = new ArrayList<>();

        String reviewLine = null;
        int nRead = 0;
        //read the subsequent lines
        while ((reviewLine = fileReader.readLine()) != null && nRead < nReviews) {
            reviews.add(new AmazonFineFoodReview(reviewLine));
            nRead++;
        }

        if(reviews.size() == 0) return null; //If no reviews were read (no more to read), return null
        return reviews;
    }
}