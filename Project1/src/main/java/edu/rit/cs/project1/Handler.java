package edu.rit.cs.project1;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;


public class Handler {
    public static final String kReturnPortLabel = "Return_Port";
    public static final String kFileBytesLabel = "File_Bytes";


    public static final String kDefServerAddress = "server";
    public static final int kDefServerPort = 7896;
    public static final int kDefRetPort = 7897;

    private static final int kFileChunkSize = 1048576; //Send files one mebibyte at a time

    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;

    //========================================<Allocation and Initialization>=========================================//

    public Handler(Socket socket) throws IOException {
        this.socket = socket;
        this.out = new DataOutputStream(socket.getOutputStream());
        this.in = new DataInputStream(socket.getInputStream());
    }

    //===============================================<Message Passing>================================================//
    public void sendBytes(byte[] bytes, int nBytes) throws IOException {
        this.out.writeInt(nBytes);
        this.out.write(bytes, 0, nBytes);

    }

    public void sendBytes(byte[] bytes) throws IOException {
        this.sendBytes(bytes, bytes.length);
    }

    public byte[] getBytes() throws IOException {
        int nBytes = this.in.readInt();
        byte[] buf = new byte[nBytes];
        this.in.readFully(buf, 0, nBytes);
        return buf;
    }

    public void sendRequest(Request request) throws IOException {
        this.sendBytes(request.toString().getBytes());
    }

    public Request getRequest() throws IOException {
        byte[] buf = getBytes();
        return Request.rebuildRequest(new String(buf, StandardCharsets.UTF_8));
    }

    public void sendFile(String filePath) throws IOException {
        // Grab and send the file size first
        long nBytes = Files.size(Paths.get(filePath));
        sendRequest(new Request(kFileBytesLabel, String.valueOf(nBytes)));

        // Create a read in buffer
        byte[] buf = new byte[kFileChunkSize];

        //Create a file input stream to read the data
        try (FileInputStream fileIn = new FileInputStream(new File(filePath))) {
            //Read and send the first block of bytes
            int result = fileIn.read(buf, 0, kFileChunkSize);
            while (result != -1) {
                sendBytes(buf, result);
                result = fileIn.read(buf, 0, kFileChunkSize);
            }
        }
    }

    public void getFile(String savePath) throws IOException {
        // Get the file size from the sender
        Request fileSizeRequest = getRequest();
        if (fileSizeRequest == null || !fileSizeRequest.getLabel().equals(kFileBytesLabel))
            throw new IOException("Partner not using file send protocol, failed to read file");

        // Save the file size as a number of bytes remaining
        long bytesRemaining = Long.parseLong(fileSizeRequest.getArgs());

        //Open a file output stream in order to save the file
        try (FileOutputStream fileOut = new FileOutputStream(new File(savePath))) {

            //While the file has some bytes remaining...
            while (bytesRemaining > 0) {
                //Get the next block of bytes
                byte[] buf = getBytes();

                //Append those bytes to the existing file
                fileOut.write(buf);

                //Subtract the number of bytes written from the total bytes remaining
                bytesRemaining -= buf.length;
            }
        }
    }

    //=================================================<Misc Getters>=================================================//
    public InetAddress getAddress() {
        return this.socket.getInetAddress();
    }

    //===================================================<Cleanup>====================================================//
    public void close() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                System.out.println("Socket close: " + e.getMessage());
            }
        }
    }
}
