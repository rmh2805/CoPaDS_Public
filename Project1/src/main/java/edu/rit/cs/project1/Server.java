package edu.rit.cs.project1;

import edu.rit.cs.project1.wordCount.WordCounter;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class Server {
    // File name stuff
    private static String workDir = "";
    private static final String kDataRootName = "affr-";
    private static final String kDataExt = ".csv";
    private static final String kCountRootName = "count-";
    private static final String kCountExt = ".txt";

    /**
     * Returns the proper file name for use by a connection
     *
     * @param connectionNr The connection's ID number (to prevent overwriting)
     * @param isDataFile   Whether or not this is a data set or the word count file
     * @return The file path for the specified file
     */
    public static String getFileName(int connectionNr, boolean isDataFile) {
        if (isDataFile)
            return workDir + kDataRootName + connectionNr + kDataExt;
        return workDir + kCountRootName + connectionNr + kCountExt;
    }

    //Connection numbering stuff
    private static int nextConnectionNr = 0;

    private static int getNextConnectionNr() {
        return nextConnectionNr++;
    }

    // Main code
    public static void main(String[] args) {
        //===========================================<Argument Parsing>===========================================//
        //=========================================<Working Directory>========================================//
        //Require a specified argument
        if (args.length < 1) {
            System.out.println("Usage Error: you must specify a working file directory");
            System.exit(1);
        }

        //Save arg[0] as the working directory
        workDir = args[0];

        //Ensure the working directory is marked as a directory
        if (!workDir.endsWith("/")) workDir = workDir + "/";

        //Ensure I have proper permissions in the working directory, exit on exception
        boolean gotException = true;
        try {
            Path testPath = Paths.get(getFileName(-1, false));
            Files.write(testPath, "foo bar baz".getBytes()); //Test that I can create and write to files
            Files.readAllBytes(testPath); //Test that I can read from files
            gotException = false;

            Files.delete(testPath); //Try and delete the test file. Not vital even if it fails
        } catch (IOException e) {
            System.out.println("Working Directory: " + e.getMessage());
        }
        if (gotException) {
            System.exit(1);
        }


        //===========================================<Server Port>============================================//
        int serverPort = Handler.kDefServerPort;
        if (args.length >= 2) {
            try {
                serverPort = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                System.out.println("Server Port: Invalid port number \"" + args[1] + "\" specified. Using default.");
                serverPort = Handler.kDefServerPort;
            }
        }


        try {
            ServerSocket listenSocket = new ServerSocket(serverPort);
            System.out.println("TCP Server is running and accepting client connections...");
            while (true) {
                Socket clientSocket = listenSocket.accept();
                Connection c = new Connection(getNextConnectionNr(), clientSocket);
            }
        } catch (IOException e) {
            System.out.println("Listen :" + e.getMessage());
        }
    }
}

class Connection extends Thread {
    private Handler handler;
    private int connectionNr;


    private void PrintStatus(String status) {
        System.out.println("\t<Connection " + this.connectionNr + ": " + status + ">");

    }

    public Connection(int connectionNr, Socket clientSocket) {
        this.connectionNr = connectionNr;
        try {
            this.handler = new Handler(clientSocket);
            this.start();
        } catch (IOException e) {
            System.out.println("Connection:" + e.getMessage());
            this.handler.close();
        }
    }

    public void run() {
        System.out.println("<Opening Connection Thread " + this.connectionNr + ">");

        boolean gotException = false; //Used to implement a 'finally-on-exception'

        //==================================<Receive Data File and Return Info>===================================//
        this.PrintStatus("Connected to client");

        //===============================<Declare Storage for Critical Values>================================//
        InetAddress retAddress = handler.getAddress(); // Grab client's address from the socket
        int retPort = Handler.kDefRetPort; // This will hold the port the client will listen on for a response

        //=========================================<Network Transfer>=========================================//
        try {
            //Set this to true here and only set false right before clean exit, so any exception will keep it set
            gotException = true;

            //Get the client's return port from the first request sent
            Request retReq = this.handler.getRequest();
            retPort = Integer.parseInt(retReq.getArgs());
            this.PrintStatus("Got return address (" + retAddress + ":" + retPort + ")");

            // Get the data file sent down the line and save it
            this.PrintStatus("Starting to receive data file");
            this.handler.getFile(Server.getFileName(this.connectionNr, true));
            this.PrintStatus("Saved data file as \"" + Server.getFileName(this.connectionNr, true) + "\"");

            // If you made it here, you got no exceptions, so clear the flag
            gotException = false;
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println("Ret Port:" + e.getMessage());
        }
        //=============================================<Cleanup>==============================================//
        finally {
            this.handler.close();
            this.handler = null;
        }
        this.PrintStatus("Disconnected from client");

        if (gotException) { //If an (unrecoverable) exception was encountered, exit this thread
            System.out.println("<Closing Connection Thread " + this.connectionNr + ">");
            return;
        }

        //==============================================<Word Count>==============================================//
        //=========================================<Count the Words>==========================================//
        this.PrintStatus("Starting word count");

        Map<String, Integer> counts = null;
        try {
            gotException = true;

            counts = WordCounter.getWordCount(Server.getFileName(this.connectionNr, true));
            gotException = false;
        } catch (IOException e) {
            System.out.println("Word Count: " + e.getMessage());
        }
        if (gotException) { //Exit if you got an exception
            System.out.println("<Closing Connection Thread " + this.connectionNr + ">");
            return;
        }

        this.PrintStatus("Finished word count");

        // Try to delete the data file, as it can be big and is no longer needed
        try {
            Files.delete(Paths.get(Server.getFileName(this.connectionNr, true)));
            this.PrintStatus("Deleted data file");
        } catch (IOException e) {
            this.PrintStatus("Failed to delete data file, continuing");
        }

        //============================================<Write Out>=============================================//
        this.PrintStatus("Saving word count");

        //Prepare to catch exceptions on file write
        gotException = true;

        //Open a print writer for the word count file
        try (PrintWriter fileOut = new PrintWriter(new File(Server.getFileName(this.connectionNr, false)))) {
            // Print a header line to the word count file
            fileOut.println("Word counts:");

            //Print each word and its count to the file
            for (String word : counts.keySet()) {
                fileOut.printf("\t%s: %d\n", word, counts.get(word));
            }

            //If you made it this far, you're clear of exceptions
            gotException = false;
        } catch (FileNotFoundException e) {
            System.out.println("Word Count File Write: " + e.getMessage());
        }
        if (gotException) { //Exit if you got an exception
            System.out.println("<Closing Connection Thread " + this.connectionNr + ">");
            return;
        }

        this.PrintStatus("Word count saved as \"" + Server.getFileName(this.connectionNr, false) + "\"");

        //======================================<Send Count File To Client>=======================================//
        try {
            //Open a connection to the count server
            this.PrintStatus("Attempting to reconnect to client");
            handler = new Handler(new Socket(retAddress, retPort));
            this.PrintStatus("Reconnected to client, starting to send count file");
            handler.sendFile(Server.getFileName(this.connectionNr, false));
            this.PrintStatus("Count file sent");
        } catch (IOException e) {
            System.out.println("Count Return: " + e.getMessage());
        } finally {
            if(this.handler != null)
                this.handler.close();
            this.PrintStatus("Disconnected from client");
        }


        //===============================================<Cleanup>================================================//
        try {
            Files.delete(Paths.get(Server.getFileName(this.connectionNr, false)));
            this.PrintStatus("Deleted count file");
        } catch (IOException e) {
            this.PrintStatus("Failed to delete count file, continuing");
        }
        System.out.println("<Closing Connection Thread " + this.connectionNr + ">");
    }
}

