# Project 1
A simple pair of programs, client and server. The client reads in a file of food reviews and sends it off to the server.
The server receives and saves the file, counts the occurrences of each unique word, and returns the results to the 
client, which then saves the file and prints it to console.

### Run the server
```
java -cp target/Project1-1.0-SNAPSHOT.jar edu.rit.cs.project1.Server <workingDirectory> [port]
```
* `workingDirectory` is a directory to store temporary files (format "affr-#.csv" and "count-#.txt") to offload from 
    heap.
* `port` is the network port to listen on. Default value is `7896`.

### Run the client
```
java -cp target/Project1-1.0-SNAPSHOT.jar edu.rit.cs.project1.Client <dataFile> <countFile> [serverAddress] [serverPort] [returnPort]
```
* `dataFile` is a csv file of food reviews, formatted like the ["Amazon fine food reviews"](https://www.kaggle.com/snap/amazon-fine-food-reviews/downloads/amazon-fine-food-reviews.zip/2)
    data set.
* `countFile` is a path to an output text file containing the results of the word count.
* `serverAddress` is the address of the count server. Default value is `server`.
* `serverPort` is the listening port on the count server. Default value is `7896`.
* `returnPort` is a port to open on the client to listen for a response. Default value is `7897`.

### Launching docker containers for project 1
To start the containers for project 1, navigate to the root directory of the repository and execute: 
```
docker-compose -f docker-compose-project1.yml up
```
In addition, the provided bash script `startProject1.sh` will execute the same command.

To build the image, either append `--build` to the above command or execute
```
docker build -f Dockerfile-Project1 -t project1:latest .
```

This will produce four containers, `server` and `client0` through `client2`. To access these use the command 
```
docker exec -it <containerName> bash
```
which will place you inside of the Project1 directory on the specified container.

From here, either use the given commands to start the server and client, or the provided `startServer.sh` and 
`startClient.sh` bash scripts in order to verify (the data file is stored as `amazon-fine-food-reviews/affr.csv`).

### Potential Upgrades
* When first connecting to the server, have the server send a reconnect port and address to the client and close 
    connection, allow for expansion to secondary servers/worker nodes.
* Apply threading to the word count
