package edu.rit.cs.gossip;

// interface defined based on this paper, https://www.distributed-systems.net/my-data/papers/2007.osr.pdf
public interface Peer<T> {

    /**
     * Select a neighboring peer to send a message
     * @return Selected peer
     */
    public Peer selectPeer();

    /**
     * Select a message to be sent
     * @return Selected message
     */
    public String selectToSend();

    /**
     * Send a selected message to a selected peer
     * @param peer Selected peer
     * @param msg Selected message
     */
    public void sendTo(Peer peer, String msg);

    /**
     * Receive a message from a peer
     * @param peer A peer who sent a message
     * @return The message content
     */
    public String receiveFrom(Peer peer);

    /**
     * Receive a message from any peer
     * @param peer Details set to match the sender peer
     * @return The received message
     */
    public String receiveFromAny(Peer peer);

    /**
     * Determine whether to cache the message to a file
     * @param cache Local cache
     * @param msg The message content
     */
    public void selectToKeep(T cache, String msg);

    /**
     * Process the data from a local cache
     * @param cache The file containing multiple messages
     */
    public void processData(T cache);

}
