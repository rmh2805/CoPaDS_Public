package Common;


import java.nio.charset.StandardCharsets;

public class Request {
    //================================================<Static Helpers>================================================//
    private static String substringBetween(String str, char startCh, char endCh) {
        int startIdx = str.indexOf(startCh);
        int endIdx = str.lastIndexOf(endCh);
        if (startIdx == -1 || endIdx == -1 || startIdx >= endIdx) {
            return null;
        }

        return str.substring(startIdx + 1, endIdx);
    }

    //===============================================<Instance Fields>================================================//
    private String label;
    private String args;
    private int listenPort;

    //===============================================<Instance Methods>===============================================//
    //=================================================<Creation>=================================================//

    /**
     * Restores the request from its string representation
     *
     * @param stringIn The string representation of a request
     * @return The recovered Request, null on failure
     */
    public static Request rebuildRequest(String stringIn) {
        if (stringIn == null) return null;

        stringIn = stringIn.trim(); //Trim off the whitespace
        if (!stringIn.startsWith(Request.class.getSimpleName())) return null;  // Check that we can cast this properly


        //Grab the contents of this request, if incomplete, return null
        String myBlock = substringBetween(stringIn, '[', ']');
        if (myBlock == null) return null;

        //Split up the data block, return null if incomplete
        String[] data = myBlock.split(",", 3);
        if (data.length < 3) return null;

        // Grab the listen port from the request
        int lPort;
        try {
            lPort = Integer.parseInt(data[1]);
        } catch (NumberFormatException ignored) {
            return null;
        }

        //Grab the label and argument strings, return null if either is missing
        String label = substringBetween(data[0], '"', '"');
        String args = substringBetween(data[2], '"', '"');
        if (label == null || args == null) return null;

        //Return a new request with the parsed label, arg block, and listen port
        return new Request(label, args, lPort);
    }

    /**
     * Creates a request with the provided label and args and the set listen port (default if cache is not initialized)
     *
     * @param label The label of the request
     * @param args  The arg block of the request
     */
    public Request(String label, String args) {
        this(label, args, (CacheSingleton.isInit()) ? CacheSingleton.getInstance().getListenPort() : CacheSingleton.kDefListenPort);
    }

    public Request(String label, String args, int listenPort) {
        this.label = label;
        this.args = args;

        if (label == null) this.label = "";
        if (args == null) this.args = "";

        this.listenPort = listenPort;

    }

    //=================================================<Getters>==================================================//
    public String getLabel() {
        return label;
    }

    public String getArgs() {
        return args;
    }

    public int getListenPort() {
        return listenPort;
    }

    //================================================<Overrides>=================================================//
    public String toString() {
        String toReturn = this.getClass().getSimpleName() + ":[\"" + this.label + "\"," + this.listenPort + ",\"" +
                this.args + "\"]";
        return new String(toReturn.getBytes(), StandardCharsets.UTF_8);
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof Request))
            return false;
        Request o = (Request) obj;
        return o.getArgs().equals(this.args) && o.getLabel().equals(this.label);
    }
}
