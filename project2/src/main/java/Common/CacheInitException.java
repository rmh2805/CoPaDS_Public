package Common;

public class CacheInitException extends Exception {
    public CacheInitException(String loc) {
        super(loc + " tried to access an uninitialized cache. Bad caller.");
    }

    public CacheInitException() {
        super("Attempted to initialize the cache a second time, naughty naughty.");
    }
}
