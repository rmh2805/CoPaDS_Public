package Common;

import java.net.InetAddress;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class CacheSingleton {
    //=============================================<Protocol Definition>==============================================//
    //=============================================<Network Defaults>=============================================//
    public static final int kDefListenPort = 7896; //Default port to listen on

    public static final int kBroadcastListenPort = 7897; //Port to listen for new connections on

    public static final String kDefSendAddress = "0.0.0.0"; //The default address to send from (INADDR_ANY)

    public static final int kDefSendPort = 0; //The default port to send from

    //==============================================<Request Labels>==============================================//
    public static final String kHeartbeatLabel = "Ba-Dum"; //Label for heartbeats

    public static final String kPeerSyncLabel = "pSync"; //Label for peer sync requests

    public static final String kDeadNodeLabel = "Death"; //Label for death notifications

    public static final String kDeathACKLabel = "DACK"; //Label for death ACK

    public static final String kDeathNACKLabel = "DNACK"; //Label for the death NACK

    public static final String kRandNumLabel = "RandNr"; //Label for random number

    //=========================================<Protocol Static Methods>==========================================//

    /**
     * Get current timestamp as the number of milliseconds from 1/1/70 (UNIX time milliseconds)
     *
     * @return The current timestamp
     */
    public static long getTimestamp() {
        return System.currentTimeMillis();
    }

    public static synchronized void printStatus(String caller, String status) {
        System.out.println("\t<" + caller + ": " + status + ">");
    }

    //============================================<Protocol Constants>============================================//
    //=========================================<Heartbeat Constants>==========================================//
    public static final long kHeartbeatDelay = 2_000L; //Number of milliseconds between heartbeats

    public static final int kHeartbeatTimeout = 500; //Number of milliseconds before a heartbeat connection fails

    public static final int kNumHeartbeatPeers = 2; //The number of peers to send your heartbeat to

    //===========================================<Death Constants>============================================//
    public static final long kPeerDeathTime = 10_000L; //Number of milliseconds before a peer dies from timeout

    public static final int kNumDeathPeers = 2; //The number of peers to send your death notifications to

    public static final int kDeathTimeout = 500; //The number of milliseconds before an outgoing death connection fails

    //==========================================<Tourney Constants>===========================================//
    public static final long kTourneyRoundTime = 5_000L; //Number of milliseconds for each round of the tournament

    public static final long kTourneyDelay = 250L; //Number of milliseconds to delay between each tourney transmission

    public static final int kTourneyTimeout = 500; //Number of milliseconds before outgoing tourney connection fails

    public static final int kMinRandNr = 1; //The minimum random number to generate in each tourney round

    public static final int kMaxRandNr = 100; //The max random number to generate in each tourney round

    public static final String kTourneyLock = "Tourney Lock";

    //=========================================<Broadcast Constants>==========================================//
    public static final long kBroadcastDelay = 2_000L; //Number of milliseconds between broadcasts

    //==========================================<Passive Constants>===========================================//
    public static final int kBroadcastListenTimeout = 1500; //Wait for a new message for at most 1.5 seconds

    //==============================================<Singleton Methods>===============================================//

    private static CacheSingleton cache = null;

    /**
     * Initialize the singleton on the default listen port
     */
    public static void init() throws CacheInitException {
        init(kDefListenPort);
    }

    /**
     * Initialize the singleton with local listening port "listenPort"
     *
     * @param listenPort The port to listen for incoming connections
     */
    public static void init(int listenPort) throws CacheInitException {
        if (cache != null) throw new CacheInitException("Cache Init");
        cache = new CacheSingleton(listenPort);
    }

    /**
     * Returns true when the singleton has been initialized
     *
     * @return true if initialized, false otherwise
     */
    public static boolean isInit() {
        return cache != null;
    }

    /**
     * Returns the current active cache instance
     *
     * @return The current active cache instance
     */
    public static CacheSingleton getInstance() {
        return cache;
    }

    //===================================================<Instance>===================================================//
    //==============================================<Member Fields>===============================================//
    private int listenPort; //The port to listen for incoming requests

    private AtomicBoolean hasStopped; //Used to track if the whole mess has stopped

    private ConcurrentMap<String, Long> peers; //A map from "<address>:<listenPort>" to last timestamp

    private long tourneyStartTime;

    private long roundNr;

    private int myNr;

    private int maxNr;

    private String maxNrOrigin;

    //=============================================<Member Functions>=============================================//
    //============================================<Initialization>============================================//

    /**
     * Internal constructor for the cache singleton
     *
     * @param listenPort The port to listen on locally
     */
    private CacheSingleton(int listenPort) {
        this.listenPort = listenPort;
        this.peers = new ConcurrentHashMap<>();
        this.hasStopped = new AtomicBoolean(false);

        synchronized (kTourneyLock) {
            this.tourneyStartTime = getTimestamp();
            startRound();
        }
    }

    //=========================================<Tourney Manipulation>=========================================//

    /**
     * Returns the current tourney round. You should hold kTourneyLock before entering.
     *
     * @return The current valid round number
     */
    private long getCurrentRound() {
        return (getTimestamp() - this.tourneyStartTime) / kTourneyRoundTime;
    }

    /**
     * Restarts all round critical values. You should hold kTourneyLock before entering
     */
    private void startRound() {
        this.roundNr = getCurrentRound();
        this.myNr = kMinRandNr + (int) (Math.random() * ((kMaxRandNr - kMinRandNr) + 1));
        this.maxNr = this.myNr;
        this.maxNrOrigin = null;
    }

    /**
     * Prints the results of the current round. You should hold kTourneyLock before entering.
     */
    private void printResults() {
        printStatus("Tourney Minder", "The max number in round " + this.roundNr + " was " + this.maxNr +
                " from " + ((this.maxNrOrigin == null) ? "this node." : "'" + this.maxNrOrigin + "' (This node's " +
                "number was " + this.myNr + ")"));
    }

    /**
     * Prints status information on the tourney to console
     */
    public void printTourneyStats() {
        synchronized (kTourneyLock) {
            printStatus("Tourney Minder", "Current Round: " + this.roundNr);
            printStatus("Tourney Minder", "My Number: " + this.myNr);
            printStatus("Tourney Minder", "Current Max Number: " + this.maxNr + " (" + this.maxNrOrigin + ")");

            long remainingMS = ((this.roundNr + 1) * kTourneyRoundTime + this.tourneyStartTime) - getTimestamp();
            printStatus("Tourney Minder", "Time Remaining: " + (double) (remainingMS) / 1000 + " s");
        }
    }

    //========================================<Peer List Manipulation>========================================//

    /**
     * Generates a key from the address and port of a peer
     *
     * @param address The ip address of the peer
     * @param port    The listen port of the peer
     * @return The key for that peer
     */
    public static String getHandle(String address, int port) {
        return address + ":" + port;
    }

    public static String getHandle(InetAddress address, int port) {
        return getHandle(address.toString().split("/")[1], port);
    }

    /**
     * Grab at most n randomly selected peers, excluding 'excludeHandle'
     *
     * @param nPeers        The max number of peers to grab
     * @param excludeHandle Any handles to exclude
     * @return The list of randomly selected peers
     */
    public List<String> getPeers(int nPeers, String excludeHandle) {
        List<String> toReturn = new LinkedList<>();

        List<String> handles = Arrays.asList(this.peers.keySet().toArray(new String[0]));
        Collections.shuffle(handles);

        for (int i = 0; i < nPeers && i < handles.size(); i++) {
            if (handles.get(i).equals(excludeHandle)) {
                nPeers++; //Skip the ignored handle and grab an extra peer to replace it
                continue;
            }

            toReturn.add(handles.get((i)));
        }

        return toReturn;
    }

    /**
     * Tries to add a new peer at `address`:`port`.
     *
     * @param address The address of the new peer
     * @param port    The port of the new peer
     */
    public void peerPing(InetAddress address, int port) {
        this.peerPing(address.toString().split("/")[1], port);
    }

    /**
     * Tries to add a new peer at `address`:`port`.
     *
     * @param address The ip address of the new peer
     * @param port    The port of the new peer
     */
    public void peerPing(String address, int port) {
        String handle = getHandle(address, port);

        if (this.peers.containsKey(handle))
            return;

        this.peers.put(handle, getTimestamp());
        printStatus("Cache", "New peer '" + handle + "' from broadcast");
    }

    /**
     * Updates the timestamp of the specified peer
     *
     * @param address The address of the peer
     * @param port    The port of the peer
     */
    private void peerUpdate(InetAddress address, int port) {
        this.peerUpdate(address.toString().split("/")[1], port);
    }

    /**
     * Updates the timestamp of the specified peer
     *
     * @param address The address of the peer
     * @param port    The port of the peer
     */
    private void peerUpdate(String address, int port) {
        String handle = getHandle(address, port);
        if (!this.peers.containsKey(handle))
            printStatus("Cache", "Got new peer at '" + handle + "' from message.");

        this.peers.put(handle, getTimestamp());
    }

    /**
     * Merge a rumored peer into the peer list
     *
     * @param handle    The handle of the rumored peer
     * @param timestamp Its last reported timestamp
     */
    private void peerMerge(String handle, long timestamp) {
        if (!this.peers.containsKey(handle)) {
            printStatus("Cache", "Got new peer at '" + handle + "' from merge");
            this.peers.put(handle, timestamp);
        } else {
            this.peers.put(handle, Math.max(timestamp, this.peers.get(handle)));
        }
    }

    /**
     * Remove a peer from the peer list
     *
     * @param handle The handle of the peer to remove
     */
    public void rmPeer(String handle) {
        if (this.peers.containsKey(handle))
            CacheSingleton.printStatus("Cache", "Removed peer '" + handle + "'");

        this.peers.remove(handle);
    }

    /**
     * Used to check if a peer is dead or unknown
     *
     * @param handle The handle of the peer to check
     * @return True if the referenced peer is not known to be alive
     */
    private boolean peerDead(String handle) {
        //If the peer is unknown, return true
        if (!this.peers.containsKey(handle)) return true;

        //Return true if the peer has timed out, false otherwise
        return Math.abs(this.peers.get(handle) - getTimestamp()) >= kPeerDeathTime;
    }

    /**
     * Returns the handle of the first dead peer encountered, null if none was found
     *
     * @return The handle of the first dead peer encountered, null if none was found
     */
    public String getDeadPeer() {
        for (String handle : this.peers.keySet()) {
            if (this.peerDead(handle)) return handle; //If you found a dead peer, return its handle
        }

        return null; //If you didn't find a dead peer, return null
    }

    public void printPeers() {
        printStatus("Cache", this.peers.keySet().toString());
    }

    //==========================================<Request Generation>==========================================//

    /**
     * Generates a heartbeat request
     *
     * @return A heartbeat request
     */
    public Request generateHeartbeat() {
        return new Request(kHeartbeatLabel, null);
    }

    /**
     * Generate a death notification request
     *
     * @return A death notification request (null if no dead node was found)
     */
    public Request generateDeadPeerRequest() {
        String peer = this.getDeadPeer(); //Get a dead peer

        if (peer == null) return null;

        return new Request(kDeadNodeLabel, peer);
    }

    public Request generateTourneyRequest() {
        String base = this.maxNr + ";" + this.tourneyStartTime + ";" + this.roundNr;

        return new Request(kRandNumLabel, base + ((this.maxNrOrigin == null) ? "" : ";" + this.maxNrOrigin));
    }

    public Request generateResponse(Request incidentRequest, InetAddress address) {
        switch (incidentRequest.getLabel()) {
            case kRandNumLabel:
                return generateTourneyRequest();

            case kDeadNodeLabel:
                String deadHandle = incidentRequest.getArgs();
                if (this.peerDead(deadHandle))
                    return new Request(kDeathACKLabel, null);
                else
                    return new Request(kDeathNACKLabel, null);

            case kHeartbeatLabel:
            default:
                return generateHeartbeat();
        }
    }

    //===========================================<Getters & Setters>==========================================//

    public void updateCache(Request incidentRequest, InetAddress sender) {
        peerUpdate(sender, incidentRequest.getListenPort());

        switch (incidentRequest.getLabel()) {
            case kRandNumLabel:
                String[] data = incidentRequest.getArgs().split(";");
                int randNr = Integer.parseInt(data[0]);
                long tourneyStartTime = Long.parseLong(data[1]);
                long roundNr = Long.parseLong(data[2]);
                String origin = (data.length >= 4) ? data[3] : getHandle(sender, incidentRequest.getListenPort());

                synchronized (kTourneyLock) {
                    if (roundNr == this.roundNr && randNr > this.maxNr) {
                        this.maxNr = randNr;
                        this.maxNrOrigin = origin;
                    }

                    this.tourneyStartTime = Math.min(this.tourneyStartTime, tourneyStartTime);
                    long currentRoundNr = getCurrentRound();
                    if (currentRoundNr != this.roundNr) {
                        printResults();
                        startRound();
                    }

                    if (roundNr == this.roundNr && randNr > this.maxNr) {
                        this.maxNr = randNr;
                        this.maxNrOrigin = origin;
                    }
                }

                break;

            case kDeadNodeLabel:
                if (this.peerDead(incidentRequest.getArgs())) this.rmPeer(incidentRequest.getArgs());
                break;

            case kDeathACKLabel: //No new information gained on my end
            case kDeathNACKLabel: //If it's still alive, I can pick it up from broadcasts
            case kHeartbeatLabel: //Nothing further needed for heartbeat
                break;

            default: //Don't know how to handle an unknown request. Notify and exit
                System.out.println("Cache Update: Unknown request label '" + incidentRequest.getLabel() + "'.");
                break;
        }
    }


    /**
     * Get the request listen port
     *
     * @return The request listen port
     */
    public int getListenPort() {
        return listenPort;
    }

    /**
     * Used to set the global stop flag
     */
    public void setStop() {
        this.hasStopped.set(true);
    }

    /**
     * Returns true when the system has been stopped
     *
     * @return true when the system has been stopped, false otherwise
     */
    public boolean isStopped() {
        return this.hasStopped.get();
    }
}
