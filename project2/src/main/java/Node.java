import Common.CacheInitException;
import Common.CacheSingleton;
import Gossiping.DeathThread;
import Gossiping.HeartbeatThread;
import Gossiping.PassiveThread;
import Gossiping.TourneyThread;
import PeerDiscovery.BroadcastListenThread;
import PeerDiscovery.BroadcastThread;

import java.io.IOException;

public class Node {
    private static final char kQuitChar = '`';
    private static final char kPeerChar = 'p';
    private static final char kTourneyChar = 't';

    public static void printStatus(String status) {
        CacheSingleton.printStatus("Main Thread", status);
    }

    public static void main(String[] args) {
        //==============================================<Initialization>==============================================//
        int listenPort = -1;
        if (args.length >= 1) {
            try {
                listenPort = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                System.out.println("Specified listen port \"" + args[0] + "\" is not a valid integer. Reverting to default");
                listenPort = -1;
            }
        }

        //Set the listen port to the parsed listen port, or the default if none was specified
        try {
            if (listenPort == -1) CacheSingleton.init();
            else CacheSingleton.init(listenPort);
        } catch (CacheInitException e) {
            System.out.println("Cache Init: " + e.getMessage());
            System.exit(1);
        }
        printStatus("Cache initialized");

        // Grab the proper send address (used to avoid broadcast to loopback on Windows)
        String sendAddress = (args.length >= 2) ? args[1] : null;


        //=============================================<Generate Threads>=============================================//
        boolean gotExceptions = false; //Used to track failed thread generation
        //=======================================<Create Heartbeat Thread>========================================//
        printStatus("Creating the heartbeat thread");
        HeartbeatThread heartbeatThread = null;

        try {
            heartbeatThread = new HeartbeatThread();
        } catch (CacheInitException e) {
            System.out.println("Heartbeat Thread Generation: " + e.getMessage());
            System.exit(1);
        }

        //======================================<Create Death Minder Thread>======================================//
        printStatus("Creating the death minder thread");
        DeathThread deathThread = null;

        try {
            deathThread = new DeathThread();
        } catch (CacheInitException e) {
            System.out.println("Death Thread Generation: " + e.getMessage());
            System.exit(1);
        }

        //========================================<Create Tourney Thread>=========================================//
        printStatus("Creating the tourney thread");
        TourneyThread tourneyThread = null;
        try {
            tourneyThread = new TourneyThread();
        } catch (CacheInitException e) {
            System.out.println("Tourney Thread Generation: " + e.getMessage());
            System.exit(1);
        }

        //=======================================<Create Broadcast Thread>========================================//
        printStatus("Creating the broadcaster thread");
        BroadcastThread broadcastThread = null;

        gotExceptions = true;
        try {
            broadcastThread = new BroadcastThread(sendAddress);
            gotExceptions = false;
        } catch (Exception e) {
            System.out.println("Broadcast Thread Generation: " + e.getMessage());
            System.exit(1);
        }

        //=======================================<Create Broadcast Thread>========================================//
        printStatus("Creating the broadcast listener thread");
        BroadcastListenThread broadcastListenThread = null;

        gotExceptions = true;
        try {
            broadcastListenThread = new BroadcastListenThread(sendAddress);
            gotExceptions = false;
        } catch (Exception e) {
            System.out.println("Broadcast Listen Thread Generation: " + e.getMessage());
        }
        if (gotExceptions) { //If you failed to generate the broadcast listen thread...
            broadcastThread.stop();
            System.exit(1);
        }

        //========================================<Create Passive Thread>=========================================//
        printStatus("Creating the passive thread");
        PassiveThread passiveThread = null;

        gotExceptions = true;
        try {
            passiveThread = new PassiveThread();
            gotExceptions = false;
        } catch (Exception e) {
            System.out.println("Passive Thread Generation: " + e.getMessage());
        }
        if (gotExceptions) { //If you failed to generate the broadcast listen thread...
            broadcastThread.stop();
            broadcastListenThread.stop();
            System.exit(1);
        }


        //==========================================<Start the Threads>===========================================//
        printStatus("Starting the threads");
        new Thread(tourneyThread).start(); //Start the tourney thread
        new Thread(heartbeatThread).start(); //Start the heartbeat thread
        new Thread(deathThread).start(); //Start the death minder thread

        new Thread(broadcastThread).start(); //Start the broadcast thread
        new Thread(broadcastListenThread).start(); //Start the broadcast listen thread
        new Thread(passiveThread).start(); //Start the passive thread

        //================================================<Exit Delay>================================================//
        printStatus("Entering holding cycle. Enter '" + kQuitChar + "' to exit.");
        while (!CacheSingleton.getInstance().isStopped()) {
            try {
                if (System.in.available() > 0) {
                    int read = System.in.read();
                    if (read == kQuitChar)
                        CacheSingleton.getInstance().setStop(); //Signal all threads to stop
                    else if (read == kPeerChar)
                        CacheSingleton.getInstance().printPeers(); //Print the peer list
                    else if (read == kTourneyChar)
                        CacheSingleton.getInstance().printTourneyStats(); //Print tourney stats
                }
            } catch (IOException e) {
                System.out.println("Quit Loop: " + e.getMessage());
            }
        }
        printStatus("Exiting");

    }
}
