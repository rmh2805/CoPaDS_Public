package Gossiping;

import Common.CacheInitException;
import Common.CacheSingleton;
import Common.Request;
import SocketHandling.TCPHandler;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class PassiveThread implements Runnable {
    private static final int kListenTimeout = 2000; //Timeout the listen socket after 2 seconds

    private ServerSocket listenSocket;

    //===============================================<Instance Methods>===============================================//
    //=============================================<Init and Cleanup>=============================================//

    public PassiveThread() throws CacheInitException, IOException {
        if (!CacheSingleton.isInit()) throw new CacheInitException("Passive thread creation"); //Require cache
        this.listenSocket = new ServerSocket(CacheSingleton.getInstance().getListenPort());
        try {
            this.listenSocket.setSoTimeout(kListenTimeout);
        } catch (Exception e) { //If you fail to set a timeout, clear listen socket and fail out
            this.listenSocket.close();
            throw e;
        }
    }


    public void stop() throws IOException {
        this.listenSocket.close();
    }

    //===================================================<Main>===================================================//
    @Override
    public void run() {
        while (!CacheSingleton.getInstance().isStopped()) {
            Socket connection = null;

            InetAddress senderAddress = null;
            TCPHandler handler = null;
            try {
                connection = listenSocket.accept();
                senderAddress = connection.getInetAddress();

                handler = new TCPHandler(connection);
            } catch (SocketTimeoutException e) { //Periodically check if we've stopped
                continue;
            } catch (IOException e) {
                System.out.println("Passive incoming IO: " + e.getMessage());
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (IOException ex) {
                        System.out.println("Passive connection close: " + ex.getMessage());
                    }
                }
                continue;
            }


            Request request = null;
            try {
                request = handler.getRequest();
            } catch (IOException e) {
                System.out.println("Passive get request: " + e.getMessage());
            }
            if (request == null) {
                handler.close();
                continue;
            }

            Request response = CacheSingleton.getInstance().generateResponse(request, senderAddress);
            try {
                handler.sendRequest(response);
            } catch (IOException e) {
                System.out.println("Passive send response: " + e.getMessage());
            }

            handler.close();

            CacheSingleton.getInstance().updateCache(request, senderAddress);
        }

        this.printStatus("Exiting");
        try {
            this.stop();
        } catch (IOException e) {
            System.out.println("Passive Thread Stop" + e.getMessage());
        }

    }

    //===================================================<Misc>===================================================//
    private void printStatus(String status) {
        CacheSingleton.printStatus("Passive Thread", status);
    }

}
