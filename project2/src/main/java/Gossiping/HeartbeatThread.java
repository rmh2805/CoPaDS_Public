package Gossiping;

import Common.CacheInitException;
import Common.CacheSingleton;
import Common.Request;
import SocketHandling.TCPHandler;

import java.io.IOException;
import java.net.*;
import java.util.List;

public class HeartbeatThread implements Runnable {

    public HeartbeatThread() throws CacheInitException {
        if (!CacheSingleton.isInit()) throw new CacheInitException("Heartbeat thread");
    }

    private void printStatus(String status) {
        CacheSingleton.printStatus("Heartbeat Thread", status);
    }

    public void run() {
        while (!CacheSingleton.getInstance().isStopped()) {
            //Select a set of peers to send heartbeats to
            List<String> peers = CacheSingleton.getInstance().getPeers(CacheSingleton.kNumHeartbeatPeers, null);

            //For each peer in that set of peers
            for (String peer : peers) {
                //Allocate these for cache update
                InetAddress address = null;
                Request response = null;

                //Grab the contact information from the handle
                String[] data = peer.split(":");

                //Try to connect to the peer
                TCPHandler handler = null;
                try (Socket connection = new Socket()) {
                    connection.connect(new InetSocketAddress(data[0], Integer.parseInt(data[1])), CacheSingleton.kHeartbeatTimeout);
                    connection.setSoTimeout(CacheSingleton.kHeartbeatTimeout);

                    handler = new TCPHandler(connection);
                    address = handler.getAddress();

                    //Send the heartbeat and receive the response
                    handler.sendRequest(CacheSingleton.getInstance().generateHeartbeat());
                    response = handler.getRequest();

                } catch (ConnectException ignored) { //If you fail to connect, it will be cleaned up elsewhere
                } catch (SocketTimeoutException ignored) { //Ditto, but for timeout (Linux)
                } catch (IOException e) {
                    System.out.println("Heartbeat Handler: " + e.getMessage());
                } finally {
                    if (handler != null) handler.close(); //Try to close out the handler
                }

                //If a response was received, update the cache
                if (response != null && address != null)
                    CacheSingleton.getInstance().updateCache(response, address);

            }

            //Put the thread to sleep until the next heartbeat
            try {
                Thread.sleep(CacheSingleton.kHeartbeatDelay);
            } catch (InterruptedException ignored) {
            }
        }

        //Exit the thread
        this.printStatus("Exiting");
    }

}
