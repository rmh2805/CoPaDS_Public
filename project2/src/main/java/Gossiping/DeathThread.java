package Gossiping;

import Common.CacheInitException;
import Common.CacheSingleton;
import Common.Request;
import SocketHandling.TCPHandler;

import java.io.IOException;
import java.net.*;
import java.util.List;

public class DeathThread implements Runnable {
    public DeathThread() throws CacheInitException {
        if (!CacheSingleton.isInit()) throw new CacheInitException("Death Thread");
    }

    private void printStatus(String status) {
        CacheSingleton.printStatus("Death Thread", status);
    }

    @Override
    public void run() {
        while (!CacheSingleton.getInstance().isStopped()) {
            Request deathRequest = CacheSingleton.getInstance().generateDeadPeerRequest();

            // No dead peers found, yield for now and restart loop on return
            if (deathRequest == null) {
                Thread.yield();
                continue;
            }

            // Get the handle of the candidate dead node from the request and remove the handle
            String deadHandle = deathRequest.getArgs();
            CacheSingleton.getInstance().rmPeer(deadHandle);

            // Generate a list of peers to notify
            List<String> peers = CacheSingleton.getInstance().getPeers(CacheSingleton.kNumDeathPeers, deadHandle);

            for (String handle : peers) {
                //Allocate these for cache update
                InetAddress address = null;
                Request response = null;

                //Grab the contact information from the handle
                String[] data = handle.split(":");

                //Try to connect to the peer
                TCPHandler handler = null;
                try (Socket connection = new Socket()) {
                    connection.connect(new InetSocketAddress(data[0], Integer.parseInt(data[1])), CacheSingleton.kDeathTimeout);
                    connection.setSoTimeout(CacheSingleton.kDeathTimeout);

                    handler = new TCPHandler(connection);
                    address = handler.getAddress();

                    //Send the heartbeat and receive the response
                    handler.sendRequest(deathRequest);
                    response = handler.getRequest();

                } catch (ConnectException ignored) { //If you fail to connect, it will be cleaned up elsewhere
                } catch (SocketTimeoutException ignored) { //Ditto, but for timeout (Linux)
                } catch (IOException e) {
                    System.out.println("Death handler: " + e.getMessage());
                } finally {
                    if (handler != null) handler.close(); //Try to close out the handler
                }

                // If failed to get the response, return
                if (response == null || address == null)
                    continue;

                //If a response was received, update the cache
                CacheSingleton.getInstance().updateCache(response, address);

                if (response.getArgs().equals(CacheSingleton.kDeathNACKLabel)) {
                    break;
                }
            }

            Thread.yield();
        }
        this.printStatus("Exiting");
    }
}
