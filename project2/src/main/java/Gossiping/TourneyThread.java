package Gossiping;

import Common.CacheInitException;
import Common.CacheSingleton;
import Common.Request;
import SocketHandling.TCPHandler;

import java.io.IOException;
import java.net.*;
import java.util.List;

public class TourneyThread implements Runnable {
    public TourneyThread() throws CacheInitException {
        if (!CacheSingleton.isInit()) throw new CacheInitException("Tourney Thread");
    }

    private void printStatus(String status) {
        CacheSingleton.printStatus("Tourney Thread", status);
    }

    @Override
    public void run() {
        while (!CacheSingleton.getInstance().isStopped()) {
            List<String> peers = CacheSingleton.getInstance().getPeers(1, null);
            if (peers.isEmpty()) { //If there's no peers, try again later
                Thread.yield();
                continue;
            }
            String peer = peers.get(0);

            //Allocate these for cache update
            InetAddress address = null;
            Request response = null;

            //Grab the contact information from the handle
            String[] data = peer.split(":");

            //Try to connect to the peer
            TCPHandler handler = null;
            try (Socket connection = new Socket()) {
                connection.connect(new InetSocketAddress(data[0], Integer.parseInt(data[1])), CacheSingleton.kTourneyTimeout);
                connection.setSoTimeout(CacheSingleton.kTourneyTimeout);

                handler = new TCPHandler(connection);
                address = handler.getAddress();

                //Send the heartbeat and receive the response
                handler.sendRequest(CacheSingleton.getInstance().generateTourneyRequest());
                response = handler.getRequest();

            } catch (ConnectException ignored) { //If you fail to connect, it will be cleaned up elsewhere
            } catch (SocketTimeoutException ignored) { //Ditto, but for timeout (Linux)
            } catch (IOException e) {
                System.out.println("Tourney Handler: " + e.getMessage());
            } finally {
                if (handler != null) handler.close(); //Try to close out the handler
            }

            //If a response was received, update the cache
            if (response != null && address != null)
                CacheSingleton.getInstance().updateCache(response, address);

            try {
                Thread.sleep(CacheSingleton.kTourneyDelay); //Don't flood the network constantly
            } catch (InterruptedException ignored) {
            }
        }
        printStatus("Exiting");
    }
}
