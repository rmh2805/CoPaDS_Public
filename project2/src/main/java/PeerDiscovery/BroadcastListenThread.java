package PeerDiscovery;

import Common.CacheInitException;
import Common.CacheSingleton;
import SocketHandling.BroadcastReceiver;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class BroadcastListenThread implements Runnable {
    private BroadcastReceiver receiver;

    public BroadcastListenThread(String myAddress) throws SocketException, CacheInitException {
        if (!CacheSingleton.isInit()) throw new CacheInitException("Broadcast Listen Thread");
        receiver = new BroadcastReceiver(myAddress);
    }

    public void stop() {
        this.receiver.close();
    }

    private void printStatus(String status) {
        CacheSingleton.printStatus("Broadcast Listen Thread", status);
    }

    @Override
    public void run() {
        while (!CacheSingleton.getInstance().isStopped()) {
            String[] peerInfo = null;
            try {
                peerInfo = receiver.getPeerPing();
            } catch (SocketTimeoutException ignored) {
                continue; //If you just timed out, continue
            } catch (IOException e) {
                System.out.println("Broadcast Listener IO: " + e.getMessage());
                continue;
            }

            int port;
            try {
                port = Integer.parseInt(peerInfo[1].trim());
            } catch (NumberFormatException e) {
                System.out.println("Broadcast Listener Port Parse: " + e.getMessage());
                continue;
            }

            CacheSingleton.getInstance().peerPing(peerInfo[0], port);
        }

        this.printStatus("Exiting");
        this.stop();
    }
}
