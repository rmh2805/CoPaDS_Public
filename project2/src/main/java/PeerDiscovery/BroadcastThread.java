package PeerDiscovery;

import Common.CacheInitException;
import Common.CacheSingleton;
import SocketHandling.Broadcaster;

import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;

public class BroadcastThread implements Runnable {
    private Broadcaster broadcaster;

    public BroadcastThread(String sendAddress) throws CacheInitException, SocketException, UnknownHostException {
        this.broadcaster = new Broadcaster(sendAddress);
    }

    private void printStatus(String status) {
        CacheSingleton.printStatus("Broadcast Thread", status);
    }

    public void stop() {
        this.broadcaster.close();
    }

    @Override
    public void run() {
        while (!CacheSingleton.getInstance().isStopped()) {
            try {
                broadcaster.Broadcast();
            } catch (IOException e) {
                System.out.println("Failed broadcast: " + e.getMessage());
            }
            try {
                Thread.sleep(CacheSingleton.kBroadcastDelay);
            } catch (InterruptedException e) {
                System.out.println("Failed broadcast sleep: " + e.getMessage());
            }
        }

        this.printStatus("Exiting");
        this.stop();
    }
}
