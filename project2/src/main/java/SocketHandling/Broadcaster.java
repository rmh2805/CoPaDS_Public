package SocketHandling;

import Common.CacheInitException;
import Common.CacheSingleton;

import java.io.IOException;
import java.net.*;

public class Broadcaster {
    //==============================================<Protocol Constants>==============================================//
    public static final String kBroadcastPrefix = "GsPnP:";
    public static final int kPortChars = 6; // Need at most 5 numerals and a nul on top of prefix

    private final InetAddress broadcastAddress;

    //===============================================<Instance Members>===============================================//
    private DatagramSocket socket;

    //===============================================<Instance Methods>===============================================//
    //=============================================<Init and Cleanup>=============================================//
    public Broadcaster() throws CacheInitException, UnknownHostException, SocketException {
        this(CacheSingleton.kDefSendAddress);
    }

    public Broadcaster(String address) throws CacheInitException, SocketException, UnknownHostException {
        if (!CacheSingleton.isInit()) throw new CacheInitException("Broadcast beacon");

        if (address == null) address = CacheSingleton.kDefSendAddress;

        broadcastAddress = InetAddress.getByName("255.255.255.255");

        this.socket = new DatagramSocket(0, InetAddress.getByName(address));
        socket.setBroadcast(true);
    }

    public void close() {
        this.socket.close();
    }

    //================================================<Socket IO>=================================================//
    public void Broadcast() throws IOException {
        byte[] portString = (kBroadcastPrefix + CacheSingleton.getInstance().getListenPort()).getBytes();

        DatagramPacket packet = new DatagramPacket(portString, portString.length, broadcastAddress, CacheSingleton.kBroadcastListenPort);

        socket.send(packet);
    }


}
