package SocketHandling;

import Common.CacheInitException;
import Common.CacheSingleton;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

//todo Eventually come back here and figure out why Windows won't receive from Linux.
public class BroadcastReceiver {
    //===============================================<Instance Members>===============================================//
    private DatagramSocket receiveSocket;
    private String myAddress;

    //===============================================<Instance Methods>===============================================//
    //=============================================<Init and Cleanup>=============================================//
    public BroadcastReceiver(String address) throws SocketException, CacheInitException {
        if (!CacheSingleton.isInit()) throw new CacheInitException("Broadcast Receiver");
        this.receiveSocket = new DatagramSocket(CacheSingleton.kBroadcastListenPort);
        this.receiveSocket.setSoTimeout(CacheSingleton.kBroadcastListenTimeout);

        this.myAddress = (address == null) ? this.receiveSocket.getLocalAddress().toString().split("/")[1] : address;
    }

    /**
     * Close this broadcast handler
     */
    public void close() {
        this.receiveSocket.close();
    }

    //================================================<Socket IO>=================================================//

    /**
     * Returns a pair of strings describing the next new peer
     *
     * @return A string array containing the peer's address followed by its main listen port
     */
    public String[] getPeerPing() throws IOException {
        String[] out = new String[2];

        byte[] buf = new byte[Broadcaster.kPortChars + Broadcaster.kBroadcastPrefix.length()]; //Make buffer for max length message
        String address = null;
        String portString = null;

        while (portString == null) {
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            receiveSocket.receive(packet);

            address = packet.getAddress().toString().split("/")[1]; //Get the IP address of the sender

            portString = new String(packet.getData());
            if (!portString.startsWith(Broadcaster.kBroadcastPrefix) || address.equals(this.myAddress)) {
                portString = null; //Ignore datagrams without the prefix
            } else {
                portString = portString.substring(Broadcaster.kBroadcastPrefix.length()); //Grab the port number
            }
        }

        out[0] = address;
        out[1] = portString;
        return out;
    }
}
