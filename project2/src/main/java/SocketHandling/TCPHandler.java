package SocketHandling;

import Common.Request;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;


public class TCPHandler {
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;

    //========================================<Allocation and Initialization>=========================================//

    public TCPHandler(Socket socket) throws IOException {
        this.socket = socket;
        this.out = new DataOutputStream(socket.getOutputStream());
        this.in = new DataInputStream(socket.getInputStream());
    }

    //===============================================<Message Passing>================================================//
    public void sendBytes(byte[] bytes, int nBytes) throws IOException {
        this.out.writeInt(nBytes);
        this.out.write(bytes, 0, nBytes);

    }

    public void sendBytes(byte[] bytes) throws IOException {
        this.sendBytes(bytes, bytes.length);
    }

    public byte[] getBytes() throws IOException {
        int nBytes = this.in.readInt();
        byte[] buf = new byte[nBytes];
        this.in.readFully(buf, 0, nBytes);
        return buf;
    }

    public void sendRequest(Request request) throws IOException {
        this.sendBytes(request.toString().getBytes());
    }

    public Request getRequest() throws IOException {
        byte[] buf = getBytes();
        return Request.rebuildRequest(new String(buf, StandardCharsets.UTF_8));
    }

    //=================================================<Misc Getters>=================================================//
    public InetAddress getAddress() {
        return this.socket.getInetAddress();
    }

    //===================================================<Cleanup>====================================================//
    public void close() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                System.out.println("Socket close: " + e.getMessage());
            }
        }
    }
}
