# Project 2
An implementation of a toy problem using a gossiping protocol ([described here](https://www.distributed-systems.net/my-data/papers/2007.osr.pdf)). 
To this end, each node spawns 1 passive thread (receives and responds to incoming gossip messages) and 3 active threads 
(sends gossip messages), one to send periodic heartbeat messages, one to notify neighbors of dead peers, and one to 
handle the problem at hand (determine which node generated the highest number in a fixed length "round").

To discover peers, each node will spawn 2 additional threads, one which broadcasts the node's contact information 
(address and port number) on port 7897 and one which listens for these broadcasts and adds new peers to the common 
cache.

After spawning these threads, the entry thread will enter a loop, reading input from terminal. Submitting a backtick 
(`` ` ``) will signal all threads to exit, a `p` will print the set of currently known peers, and a `t` will print a 
snapshot of the current tourney state.

### Run the node
```
java -cp target/project2-1.0-SNAPSHOT.jar Node [port [interface address]]
```
* `port` is the port to listen for gossip on. Default value is `7896`
* `interface address` is the address of the network interface to broadcast on. Default value is `0.0.0.0` (`INADDR_ANY`).

For convenience (read: developer laziness), a bash script (`startNode.sh`) has been provided to initialize the node with 
default values.

### Launching docker containers for project 2
To start the containers for project 2, navigate to the root directory of the repository and execute: 
```
docker-compose -f docker-compose-project2.yml up
```
In addition, the provided bash script `startProject2.sh` will execute the same command.

To build the image, either append `--build` to the above command or execute
```
docker build -f Dockerfile-Project2 -t project2:latest .
```

This will produce three containers, `peer0` through `peer2`. To access these use the command 
```
docker exec -it <containerName> bash
```
which will place you inside of the `project2` directory on the specified container.

From here, either use the given commands to start the node, or the provided `startNode.sh`.

### Potential Upgrades
* Have nodes share peer lists as gossip (could be leveraged to bridge subnets and reduce false deaths at the cost of 
    large message sizes)
* Expand configuration by argument (e.g. include args for broadcast listen, timeouts, heartbeat frequency, etc.)
    * Shift from ordered lists of arguments to tagged arguments (e.g. `-p <gossip listen port>`)
* Code cleanup (e.g. condense the common/similar code from the active threads into a single `gossipExchange` method)
